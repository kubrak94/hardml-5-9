FROM python:3.9

RUN pip install flask requests uwsgi mlflow==1.14 scikit-learn boto3 protobuf==3.20.*

WORKDIR /app
COPY . /app

CMD ["uwsgi", "--http", "0.0.0.0:80", "--master", "-p", "1", "--threads", "1", "-w", "solution:app"]
