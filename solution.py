import os

import mlflow
import requests
from flask import Flask, Response, jsonify, request


labels = ["setosa", "versicolor", "virginica"]
mlflow.set_tracking_uri(os.environ["MLFLOW_TRACKING_URI"])
experiment_name = os.environ["MLFLOW_EXPERIMENT_NAME"]
try:
    mlflow.create_experiment(experiment_name)
except mlflow.exceptions.RestException:
    pass
mlflow.set_experiment(experiment_name)

version = 1
while True:
    MODEL_PATH = f"models:/iris_sklearn/{version}"

    try:
        loaded_model = mlflow.sklearn.load_model(MODEL_PATH)
    except mlflow.exceptions.RestException:
        MODEL_PATH = f"models:/iris_sklearn/{version - 1}"
        loaded_model = mlflow.sklearn.load_model(MODEL_PATH)
        break

    version += 1

try:
    thresholds = {
"threshold_0_1": float(os.environ["THRESHOLD_0_1"]),
"threshold_1_2": float(os.environ["THRESHOLD_1_2"])
}
except:
    thresholds = {
"threshold_0_1": None,
"threshold_1_2": None
}

app = Flask(__name__)

@app.route('/get_source_iris_pred')
def get_source_iris_pred():
    sepal_length = float(request.args.get("sepal_length"))
    sepal_width = float(request.args.get("sepal_width"))
    score = loaded_model.predict([[sepal_length, sepal_width]])[0]
    return jsonify(prediction=score)

@app.route('/get_string_iris_pred')
def get_string_iris_pred():
    sepal_length = float(request.args.get("sepal_length"))
    sepal_width = float(request.args.get("sepal_width"))
    score = loaded_model.predict([[sepal_length, sepal_width]])[0]

    try:
        if score < thresholds["threshold_0_1"]:
            class_str = labels[0]
        elif thresholds["threshold_0_1"] <= score < thresholds["threshold_1_2"]:
            class_str = labels[1]
        else:
            class_str = labels[2]

        return jsonify(**{"class": score, "class_str": class_str, "threshold_0_1": thresholds["threshold_0_1"], "threshold_1_2": thresholds["threshold_1_2"]})
    except:
        return Response("Something went wrong", status=501)

#@app.route('/set_thresholds')
#def set_thresholds():
#    d = requests.get("http://95.217.213.216:5000/get_iris_thresholds").json()
#    thresholds["threshold_0_1"] = d["threshold_0_1"]
#    thresholds["threshold_1_2"] = d["threshold_1_2"]
#    return jsonify(status="ok")
